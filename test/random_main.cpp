#include "../src/random_function.hpp"
#include "../src/random_class.hpp"

int main(int argc, char** argv)
{
    //Seed globally
    int myseed = atoi(argv[1]);
    RNG::seed(myseed);
    printRandomNumbers(1, 10, 5);
    printRandomNumbers(11, 20, 5);
    printRandomNumbers(1, 10, 5);
    return 0;
}



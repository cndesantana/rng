#ifndef _RANDOM_CLASS_HPP
#define _RANDOM_CLASS_HPP

#include<random>

typedef std::mt19937                     ENG;    // Mersenne Twister
typedef std::uniform_int_distribution<> iDIST;   // Uniform Integer Distribution

class RNG {
private:
    static ENG eng;
    iDIST idist;
public:
    static void seed(int s) { eng.seed(s); }
    RNG(int imin, int imax) : idist(imin, imax) {}
    int generate() { return idist(eng); }
};

// the one generator, stored as RNG::eng
ENG RNG::eng;

#endif

#ifndef _RANDOM_FUNCTION_HPP
#define _RANDOM_FUNCTION_HPP

#include<iostream>
#include"random_class.hpp"

// print some generated numbers from a range
void printRandomNumbers(int imin, int imax, int N){
    std::cout << "Range = [" << imin << "," << imax << "]" << std::endl;
    RNG myirn(imin, imax);
    for (int i = 0; i < N; i++){
        std::cout << myirn.generate() << std::endl;
    }
    return;
}

#endif
